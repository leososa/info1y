#include <stdio.h>
#define RMAX 10
#define CMAX 7

int contapicchi(int mappa[][CMAX], int nr, int nc){   /*ricorda di passare comunque il numero di righe e colonne*/
	int i, j, h, k, ok, cont;
    
    cont=0;
	for(i=0; i<nr; i++){               /*scorro tutto l'array , bisogna controllare ogni valore come possibile picco*/
		for(j=0; j<nc; j++){
			ok=1;
			for(h=0; h<nr && ok==1; h++){              /*questo ciclo controllo se il valore[i][j] � il maggiore della propria colonna*/
				if(mappa[i][j]<=mappa[h][j] && h!=i){  /*se trovo un numero all'interno della colonna che � maggiore-uguale del valore[i][j], e non � quel valore[i][j], (h!=i), esco dal ciclo, errore, ok=0, in modo tale che non entri nel ciclo sottostante*/
					ok=0;  /*la condizione soprastante h!=i �  necessaria perch� devo "evitare" il valore[i][j] che sto analizzando*/
				}			
			}
			for(k=0; k<nc && ok==1; k++){              /*questo ciclo controllo se il valore � il maggiore della propria riga(eseguo questo ciclo a condizione che ok sia ancora uguale a 1 */
				if(mappa[i][j]<=mappa[i][k] && k!=j){   /*stessa cosa per le riga del valore[i][j]*/
					ok=0;
				}	
			}
			if(ok==1){    /*se il programma esegue questa riga di codice, vuol dire che il valore ok � rimasto pari a 1, non � stato modificato...*/
				cont++;   /*...ci� implica che il valore[i][j] � effetivamente un picco, aumento il contatore*/
			}
		}
	}
	return cont;
}


