/*
    PER IL TESTO DELL'ESERCIZIO GUARDARE NEL PDDF
*/

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>

int leggiNumeroInteroPositivo();

int main()
{

    int i, n, interni = 0;
    float x ,y, pigreco;

    n = leggiNumeroInteroPositivo();

    srand((unsigned int)time(NULL));

    for (i=0; i<n; i++){

        x = ((float)rand())/RAND_MAX; //genera un float casualte compreso tra 0 e 1
        y = ((float)rand())/RAND_MAX;
        //printf("X: %f\n", x);
        //printf("Y: %f\n", y);

        if((pow(x,2)+pow(y,2)) < 1) //se x^2 + y^2 < 1, vuol dire che il punto cade all'interno del cerchio di raggio 1 (quindi è interno)
            interni++;
     }

     pigreco = (float)interni/n * 4; //la divisione tra il numero di punti interni e il numero di punti totali, è l'approssimazione di π/4. Quindi moltiplico per 4 per ottenere π

     printf("L'approssimazione di π è %f\n", pigreco);

    return 0;
}

int leggiNumeroInteroPositivo() {

    float num_orig;
    int num;

    do {
        printf("Quanti punti si desiderano utilizzare per l'approssimazione di π?\n");
        scanf("%f%*c", &num_orig);
        num = num_orig;
    } while(num < 1 || num != num_orig); //cicla finchè non inseriamo un numero positivo e interno

    return num;
}
